package com.ravi.jessebeauty.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by ravirane on 4/23/16.
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{

    TextView timeText;

    public void setTimeText(TextView timeText) {
        this.timeText = timeText;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        final Calendar calendar = Calendar.getInstance();
        // Get the current hour and minute
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog tpd3 = new TimePickerDialog(getActivity(),
                AlertDialog.THEME_HOLO_DARK,this,hour,minute,false);

        return tpd3;
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute){
        timeText.setText(hourOfDay + ":" + minute);
        // Do something with the returned time
      //  TextView tv = (TextView) getActivity().findViewById(R.id.tv);
      //  tv.setText("HH:MM\n" + hourOfDay + ":" + minute);
    }
}
