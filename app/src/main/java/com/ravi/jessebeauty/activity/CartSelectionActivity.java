package com.ravi.jessebeauty.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ravi.jessebeauty.R;
import com.ravi.jessebeauty.fragment.DatePickerFragment;
import com.ravi.jessebeauty.fragment.TimePickerFragment;
import com.ravi.jessebeauty.model.CartItem;

import java.util.ArrayList;
import java.util.List;

public class CartSelectionActivity extends AppCompatActivity {

    Button btnDatePicker, btnTimePicker;
    EditText txtDate, txtTime;
    private int mYear, mMonth, mDay, mHour, mMinute;
    CartSelectionActivity cartSelectionActivity;
    List<CartItem> cartItemList;
    private ListAdapter adapter;

    private class ListAdapter extends BaseAdapter {

        private Activity activity;

        public ListAdapter(Activity activity) {
            this.activity = activity;
        }

        @Override
        public int getCount() {
            return cartItemList.size();
        }

        @Override
        public Object getItem(int position) {
            return cartItemList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            CartItem cartItem = cartItemList.get(position);
            if (convertView == null) {
                int layout_id = R.layout.row_cart_item;
                LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(layout_id, null);
            }

            TextView subCatName = (TextView) convertView.findViewById(R.id.subCatName);
            subCatName.setText(cartItem.name);

            TextView subCatDetail = (TextView) convertView.findViewById(R.id.subCatDetail);
            subCatDetail.setText(cartItem.details);

            TextView subCatCost = (TextView) convertView.findViewById(R.id.subCatCost);
            subCatCost.setText(cartItem.cost);

            TextView subCatTime = (TextView) convertView.findViewById(R.id.subCatTime);
            subCatTime.setText(cartItem.time);

            final TextView btnScheduleDate = (TextView) convertView.findViewById(R.id.btnScheduleDate);

            if(cartItem.scheduleDate != null) {
                btnScheduleDate.setText(cartItem.scheduleDate);
            }

            btnScheduleDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatePickerFragment dFragment = new DatePickerFragment();
                    dFragment.setDateText(btnScheduleDate);
                    dFragment.show(getFragmentManager(), "Date Picker");
                }
            });
            final TextView btnScheduleTime = (TextView) convertView.findViewById(R.id.btnScheduleTime);
            if(cartItem.scheduleTime != null) {
                btnScheduleTime.setText(cartItem.scheduleTime);
            }
            btnScheduleTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TimePickerFragment dFragment = new TimePickerFragment();
                    dFragment.setTimeText(btnScheduleTime);
                    dFragment.show(getFragmentManager(), "Date Picker");
                }
            });
            final ImageView imageView = (ImageView) convertView.findViewById(R.id.btnDelete);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cartItemList.remove(position);
                    ListAdapter.this.notifyDataSetChanged();
                }
            });
            return convertView;
        }


        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_selection);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        cartSelectionActivity = this;
        cartItemList = new ArrayList<>();
        cartItemList.add(new CartItem("Hair cut1","Simple hair cut","30 min","150 rs",null,null));
        cartItemList.add(new CartItem("Hair cut2","Simple hair cut","30 min","150 rs",null,null));
        cartItemList.add(new CartItem("Hair cut3","Simple hair cut","30 min","150 rs",null,null));
        cartItemList.add(new CartItem("Hair cut4","Simple hair cut","30 min","150 rs",null,null));
        cartItemList.add(new CartItem("Hair cut5","Simple hair cut","30 min","150 rs",null,null));
        cartItemList.add(new CartItem("Hair cut6","Simple hair cut","30 min","150 rs",null,null));

        ListView lv = (ListView) findViewById(R.id.cartSelectionList);
        adapter = new ListAdapter(CartSelectionActivity.this);
        lv.setAdapter(adapter);

        TextView checkout = (TextView) findViewById(R.id.btnCheckout);
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CartSelectionActivity.this, CheckoutActivity.class);
                startActivity(i);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cart_selection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

}
