package com.ravi.jessebeauty.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ravi.jessebeauty.R;
import com.ravi.jessebeauty.model.Category;

import java.util.ArrayList;
import java.util.List;


public class HomeActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private NavigationView navigationView;
    List<Category> beautyCategory = new ArrayList<>();
    private ListAdapter adapter;

    private class ListAdapter extends BaseAdapter {

        private Activity activity;

        public ListAdapter(Activity activity) {
            this.activity = activity;
        }

        @Override
        public int getCount() {
            return beautyCategory.size();
        }

        @Override
        public Object getItem(int position) {
            return beautyCategory.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Category category = beautyCategory.get(position);
                if(position == 0) {
                    int layout_id = R.layout.home_row1;
                    LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    convertView = layoutInflater.inflate(layout_id, null);
                } else if(position == 1) {
                    int layout_id = R.layout.home_row2;
                    LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    convertView = layoutInflater.inflate(layout_id, null);
                } else {
                        int layout_id = R.layout.row_beauty_option;
                        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                        convertView = layoutInflater.inflate(layout_id, null);
                    int resID = getResources().getIdentifier(category.image, "drawable", getPackageName());
                    ImageView imgGameLogo = (ImageView) convertView.findViewById(R.id.imgGameLogo);
                    Glide.with(HomeActivity.this).load("").placeholder(resID).into(imgGameLogo);
                    TextView categoryText = (TextView) convertView.findViewById(R.id.myImageViewText);
                    categoryText.setText(category.title);
                }
            return convertView;
        }

        @Override
        public int getViewTypeCount() {
            return 3;
        }

        @Override
        public int getItemViewType(int position) {
            if(position == 0) {
                return 0;
            } else if(position == 1) {
                return 1;
            } else  {
                return 2;
            }
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            if(position == 0 || position ==1) {
                return false;
            }
            return true;
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        Intent intent = getIntent();
        String drawerState = intent.getStringExtra("drawerState");

   //     Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
   //     setSupportActionBar(toolbar);

        ImageView drawerLogo = (ImageView) findViewById(R.id.drawerLogo);
        drawerLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        //  final ActionBar ab = getSupportActionBar();
      //  ab.setHomeAsUpIndicator(R.drawable.ic_menu);
      //  ab.setDisplayHomeAsUpEnabled(true);


        navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if (menuItem.isChecked())
                    menuItem.setChecked(false);
                else menuItem.setChecked(true);
                Log.e("Inside Menu", "Inside Menu");
                mDrawerLayout.closeDrawers();

                switch (menuItem.getItemId()) {
                  /*  case R.id.nav_profile:
                        Intent intent = new Intent(PayDashboard.this, ProfileActivity.class);
                        FIDashBoardActivity.this.startActivity(intent);
                        return true; */
                    case R.id.nav_payreq:
                        return true;
                    case R.id.nav_wallet:

                        return true;

                    default:
                        Toast.makeText(getApplicationContext(), "Default Wrong", Toast.LENGTH_SHORT).show();
                        return true;

                }
            }
        });


         /*Drawer layout*/

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        if(drawerState != null && drawerState.equals("open")) {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }

        beautyCategory.add(new Category("a","a"));
        beautyCategory.add(new Category("b","b"));
        beautyCategory.add(new Category("img1_1","Hair Care"));
        beautyCategory.add(new Category("img2_1","Health & Massage"));
        beautyCategory.add(new Category("img3_1","Skin Care"));
        beautyCategory.add(new Category("img4_1","Pedicure & more"));
        beautyCategory.add(new Category("img5_1","Marriage & Functions"));
        beautyCategory.add(new Category("img6_1","Mens Grooming"));

        ListView lv = (ListView) findViewById(R.id.mainScreenList);
        adapter = new ListAdapter(HomeActivity.this);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intt = new Intent(HomeActivity.this,CategoryActivity.class);
                startActivity(intt);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {




            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;


        }

        return super.onOptionsItemSelected(item);
    }

}
