package com.ravi.jessebeauty.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ravi.jessebeauty.R;
import com.ravi.jessebeauty.model.Category;
import com.ravi.jessebeauty.model.SubCategory;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CategoryActivity extends AppCompatActivity {


    Map<Category,List<SubCategory>> categoryMap = new LinkedHashMap<>();
    List<Object> categoryObject = new ArrayList<>();
    private ListAdapter adapter;

    private class ListAdapter extends BaseAdapter {

        private Activity activity;

        public ListAdapter(Activity activity) {
            this.activity = activity;
        }

        @Override
        public int getCount() {
            return categoryObject.size();
        }

        @Override
        public Object getItem(int position) {
            return categoryObject.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Object obj = categoryObject.get(position);

            if(obj instanceof Category) {
                int layout_id = R.layout.category_header;
                LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(layout_id, null);
                Category category = (Category) categoryObject.get(position);
                TextView categoryText = (TextView) convertView.findViewById(R.id.categoryName);
                categoryText.setText(category.title);
            } else {
                int layout_id = R.layout.category_nonheader;
                LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(layout_id, null);

                final SubCategory subCategory = (SubCategory) categoryObject.get(position);
                TextView subCatName = (TextView) convertView.findViewById(R.id.subCatName);
                subCatName.setText(subCategory.name);
                TextView subCatDetail = (TextView) convertView.findViewById(R.id.subCatDetail);
                subCatDetail.setText(subCategory.details);
                TextView subCatCost = (TextView) convertView.findViewById(R.id.subCatCost);
                subCatCost.setText(subCategory.cost);
                TextView subCatTime = (TextView) convertView.findViewById(R.id.subCatTime);
                subCatTime.setText(subCategory.time);
                final ImageView imageView = (ImageView) convertView.findViewById(R.id.btnAdd);
                if(subCategory.isSelected) {
                    imageView.setImageResource(R.drawable.delete);
                } else {
                    imageView.setImageResource(R.drawable.add);
                }
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String imgId = (String) imageView.getTag();
                        switch(imgId) {
                            case "0":
                                imageView.setImageResource(R.drawable.delete);
                                imageView.setTag("1");
                                subCategory.isSelected = true;
                                break;
                            case "1":
                                imageView.setImageResource(R.drawable.add);
                                imageView.setTag("0");
                                subCategory.isSelected = false;
                                break;
                        }
                    }
                });

            }
            return convertView;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            Object obj = getItem(position);
            if(obj instanceof Category) {
                 return 0;
            } else {
                return 1;
            }
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);


        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        populateListCategory();

        ListView lv = (ListView) findViewById(R.id.categoryList);
        adapter = new ListAdapter(CategoryActivity.this);
        lv.setAdapter(adapter);
    }

    private void populateListCategory() {
        Category category = new Category("","Hair Care");
        categoryObject.add(category);
        categoryObject.add(new SubCategory("Hair cut","Simple hair cut","30 min","150 rs"));
        categoryObject.add(new SubCategory("Hair cut","Simple hair cut","30 min","150 rs"));
        categoryObject.add(new SubCategory("Hair cut","Simple hair cut","30 min","150 rs"));
        categoryObject.add(new SubCategory("Hair cut", "Simple hair cut", "30 min", "150 rs"));


        category = new Category("","Hair Care");
        categoryObject.add(category);
        categoryObject.add(new SubCategory("Hair cut","Simple hair cut","30 min","150 rs"));
        categoryObject.add(new SubCategory("Hair cut","Simple hair cut","30 min","150 rs"));
        categoryObject.add(new SubCategory("Hair cut","Simple hair cut","30 min","150 rs"));


        category = new Category("","Hair Care");
        categoryObject.add(category);
        categoryObject.add(new SubCategory("Hair cut","Simple hair cut","30 min","150 rs"));
        categoryObject.add(new SubCategory("Hair cut","Simple hair cut","30 min","150 rs"));
        categoryObject.add(new SubCategory("Hair cut","Simple hair cut","30 min","150 rs"));
        categoryObject.add(new SubCategory("Hair cut", "Simple hair cut", "30 min", "150 rs"));
        categoryObject.add(new SubCategory("Hair cut", "Simple hair cut", "30 min", "150 rs"));



        category = new Category("","Hair Care");
        categoryObject.add(category);
        categoryObject.add(new SubCategory("Hair cut","Simple hair cut","30 min","150 rs"));
        categoryObject.add(new SubCategory("Hair cut","Simple hair cut","30 min","150 rs"));
        categoryObject.add(new SubCategory("Hair cut","Simple hair cut","30 min","150 rs"));
        categoryObject.add(new SubCategory("Hair cut", "Simple hair cut", "30 min", "150 rs"));


        category = new Category("","Hair Care");
        categoryObject.add(category);
        categoryObject.add(new SubCategory("Hair cut","Simple hair cut","30 min","150 rs"));
        categoryObject.add(new SubCategory("Hair cut","Simple hair cut","30 min","150 rs"));
        categoryObject.add(new SubCategory("Hair cut","Simple hair cut","30 min","150 rs"));
        categoryObject.add(new SubCategory("Hair cut", "Simple hair cut", "30 min","150 rs"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_category, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.cart) {
            //    showSpinner(true, true);
            Intent intt = new Intent(CategoryActivity.this, CartSelectionActivity.class);
            startActivity(intt);

        }

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }
}
