package com.ravi.jessebeauty.model;

/**
 * Created by ravirane on 4/19/16.
 */
public class Category {

    public String image;
    public String title;

    public Category(String image, String title) {
        this.image = image;
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        return title.equals(category.title);

    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }
}
