package com.ravi.jessebeauty.model;

/**
 * Created by ravirane on 4/23/16.
 */
public class CartItem {

    public String name;
    public String details;
    public String time;
    public String cost;
    public String scheduleDate;
    public String scheduleTime;

    public CartItem(String name, String details, String time, String cost, String scheduleDate, String scheduleTime) {
        this.name = name;
        this.details = details;
        this.time = time;
        this.cost = cost;
        this.scheduleDate = scheduleDate;
        this.scheduleTime = scheduleTime;
    }
}
