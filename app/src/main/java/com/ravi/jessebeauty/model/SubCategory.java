package com.ravi.jessebeauty.model;

/**
 * Created by ravirane on 4/22/16.
 */
public class SubCategory {
    public String name;
    public String details;
    public String time;
    public String cost;
    public boolean isSelected = false;

    public SubCategory(String name, String details, String time, String cost) {
        this.name = name;
        this.details = details;
        this.time = time;
        this.cost = cost;
    }
}
